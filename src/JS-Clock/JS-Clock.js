import moment from "moment";

const currentTimeH3 = document.getElementById("current-time");
function updateTime() {
    currentTimeH3.innerHTML = moment().format('MMMM Do YYYY, h:mm:ss a');
}
setInterval(updateTime, 1000);


