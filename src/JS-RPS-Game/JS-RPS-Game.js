
const easyButton = document.getElementById("easy-button");
const mediumButton = document.getElementById("medium-button");
const hardButton = document.getElementById("hard-button");

const rockButton = document.getElementById("rock-button");
const paperButton = document.getElementById("paper-button");
const sciButton = document.getElementById("scissors-button");
const winButton = document.getElementById("win-button");

const resetButton = document.getElementById("reset-button");

const gameButtons = document.getElementById("game-buttons-div");
const result = document.getElementById("result-div");

const resultText = document.getElementById("result-text");
const youPicked = document.getElementById("you-picked-text");
const botPicked = document.getElementById("bot-picked-text");

gameButtons.style.display = 'none';
result.style.display = 'none';
winButton.style.display = 'none';

let difficulty = "";
let failureCounter = 0;

easyButton.onclick = () => {
    difficulty = "Easy";
    console.log("Easy clicked");
    console.log(difficulty);
    gameButtons.style.display = 'flex';
    gameButtons.style.border = '3px solid forestgreen';

};

mediumButton.onclick = () => {
    difficulty = "Medium";
    console.log("Medium clicked");
    console.log(difficulty);
    gameButtons.style.display = 'flex';
    gameButtons.style.border = '3px solid blue';

};

hardButton.onclick = () => {
    difficulty = "Hard";
    console.log("Hard clicked");
    console.log(difficulty);
    gameButtons.style.display = 'flex';
    gameButtons.style.border = '3px solid darkred';

};

rockButton.onclick = () => {
    startGame(rockButton.innerText);
};

paperButton.onclick = () => {
    startGame(paperButton.innerText);
};

sciButton.onclick = () => {
    startGame(sciButton.innerText);
};

winButton.onclick = () => {
    startGame(winButton.innerText);
};

function startGame(buttonClicked){

    switch (difficulty) {
        case "Easy":
            easyGame(buttonClicked);
            result.style.display = 'flex';
            break;
        case "Medium":
            mediumGame(buttonClicked);
            result.style.display = 'flex';
            break;
        case "Hard":
            hardGame(buttonClicked);
            result.style.display = 'flex';
            break;
    }
}

function easyGame(buttonClicked) {
    youPicked.innerHTML = `You picked ${buttonClicked}`;
    botPicked.innerHTML = "The bot gave up!";
    resultText.innerHTML = "You win!";
}

function mediumGame(buttonClicked){
    const alternatives = ['Rock', 'Paper', 'Scissors'];
    const botChose = alternatives[Math.floor(Math.random() * alternatives.length)];

    youPicked.innerHTML = `You picked ${buttonClicked}`;
    botPicked.innerHTML = `The bot picked: ${botChose}`;

    switch (buttonClicked) {
        case 'Rock':
            if(botChose === "Rock"){
                resultText.innerHTML = "Tie!";
            } else if (botChose === "Scissors"){
                resultText.innerHTML = "You win!";
            } else if (botChose === "Paper"){
                resultText.innerHTML = "You loose!";
            }
            break;
        case 'Paper':
            if(botChose === "Rock"){
                resultText.innerHTML = "You win!";
            } else if (botChose === "Scissors"){
                resultText.innerHTML = "You loose!";
            } else if (botChose === "Paper"){
                resultText.innerHTML = "Tie!";
            }
            break;
        case 'Scissors':
            if(botChose === "Paper"){
                resultText.innerHTML = "You win!";
            } else if (botChose === "Rock"){
                resultText.innerHTML = "You loose!";
            } else if (botChose === "Scissors"){
                resultText.innerHTML = "Tie!";
            }
            break;
    }
}

function hardGame(buttonClicked){

    youPicked.innerHTML = `You picked ${buttonClicked}`;
    botQuotes = ['Pathetic performance', 'Are you even trying?', 'Humans are so bad at this!', 'So easy!', 'Hahaha wow that was bad', 'Easiest game of my robot life!', 'Give up already!'];
    const botChose = botQuotes[Math.floor(Math.random() * botQuotes.length)];

    switch (buttonClicked) {
        case 'Rock':
            if(failureCounter >= 10){
                winButton.style.display = 'block';
                botPicked.innerHTML = 'The bot picked paper!';
                resultText.innerHTML = botChose;
            } else {
                botPicked.innerHTML = 'The bot picked paper!';
                resultText.innerHTML = botChose;
                failureCounter++;
            }
            break;
        case 'Paper':
            if(failureCounter >= 10){
                winButton.style.display = 'block';
                botPicked.innerHTML = 'The bot picked scissors!';
                resultText.innerHTML = botChose;
            } else {
                botPicked.innerHTML = 'The bot picked scissors!';
                resultText.innerHTML = botChose;
                failureCounter++;
            }
            break;
        case 'Scissors':
            if(failureCounter >= 10){
                winButton.style.display = 'block';
                botPicked.innerHTML = 'The bot picked rock!';
                resultText.innerHTML = botChose;
            } else {
                botPicked.innerHTML = 'The bot picked rock!';
                resultText.innerHTML = botChose;
                failureCounter++;
            }
            break;
        case 'HK416N Automatic Rifle':
                botPicked.innerHTML = 'Bullets! My only weakness!';
                resultText.innerHTML = 'You win!';
            break;
    }
}

resetButton.onclick = () => {
    result.style.display = 'none';
    winButton.style.display = 'none';
    failureCounter = 0;

};
